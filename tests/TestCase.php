<?php
namespace Conceptlz\Zeus\Thunderbolt\Test;

use Conceptlz\Zeus\Thunderbolt\Models\User;
use Orchestra\Testbench\TestCase as OrchestraTestCase;
use Conceptlz\Zeus\Thunderbolt\ThunderboltServiceProvider;
use Spatie\Permission\Models\Role;
use Spatie\Permission\PermissionServiceProvider;

class TestCase extends OrchestraTestCase
{
    /**
     * Setup the test environment.
     */
    public function setUp()
    {
        parent::setUp();
        $this->loadLaravelMigrations(['--database' => 'testbench']);
    }

    /**
     * Login user
     *
     * @param null $user
     * @return $this
     */
    public function loginUser($user = null)
    {
        if( $user ) {
           return $this->actingAs($user);
        }

        $user = $this->createUser(['name' => 'Moiz Boss']);

        return $this->actingAs($user);
    }

    /**
     * Create a user
     *
     * @param array $attributes
     * @return mixed
     */
    public function createUser($attributes = [])
    {
        return User::create( array_merge($attributes, [
            'name' => 'Moiz',
            'email' => 'email@example.com',
            'password' => bcrypt('secret')
        ]));
    }

    public function syncRolesFromConfig()
    {
        if( config('thunderbolt.auth.enable_role_permission') ) {

            $roles = config('thunderbolt.auth.roles', []);
            foreach ($roles as $role) {
                Role::firstOrCreate(['name' => $role]);
            }
        }
    }

    /**
     * Load package service provider
     * @param  \Illuminate\Foundation\Application $app
     * @return array
     */
    protected function getPackageProviders($app)
    {
        return [
            ThunderboltServiceProvider::class,
            PermissionServiceProvider::class
        ];
    }

    /**
     * Define environment setup.
     *
     * @param  \Illuminate\Foundation\Application  $app
     * @return void
     */
    protected function getEnvironmentSetUp($app)
    {
        // Setup default database to use sqlite :memory:
        $app['config']->set('database.default', 'testbench');
        $app['config']->set('database.connections.testbench', [
            'driver'   => 'sqlite',
            'database' => ':memory:',
            'prefix'   => '',
        ]);

        // set mail setting
        $app['config']->set('mail.username', 'a08aca178b96de');
        $app['config']->set('mail.password', 'a472edf5e087de');
        $app['config']->set('mail.host', 'mailtrap.io');
        $app['config']->set('mail.port', 2525);

        // set auth provider model
        $app['config']->set('auth.providers.users.model', User::class);
    }
}