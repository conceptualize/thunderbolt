<?php
namespace Conceptlz\Zeus\Thunderbolt\Test\Auth;

use Carbon\Carbon;
use Conceptlz\Zeus\Thunderbolt\Mail\ConfirmationEmail;
use Illuminate\Support\Facades\Mail;
use Conceptlz\Zeus\Thunderbolt\Models\User;
use Conceptlz\Zeus\Thunderbolt\Test\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Spatie\Permission\Models\Role;

class AuthTest extends TestCase
{
    use RefreshDatabase;

    /**
    * guest can visit register page
    *
    * @test
    */
    public function guest_can_visit_register_page()
    {
        // guest can visit login page
        $this->get(\route('register'))
            ->assertStatus(200)
            ->assertSee('Register');

        // if registration is not allowed from config redirect to login
        config()->set('thunderbolt.auth.allow_registration', false);
        $this->get(\route('register'))
            ->assertRedirect(route('login'))
            ->assertSessionHas('status', 'Registration is not allowed. Ask admin to create an account for you.');

        // logged in user cant
        $this->loginUser();
        $this->get(\route('register'))
            ->assertRedirect(\config('thunderbolt.auth.redirect_to'));
    }

    /**
    * user can register with name email and a password
    *
    * @test
    */
    public function user_can_register_with_name_email_and_a_password()
    {
        $payload = [
            'name' => 'John Doe',
            'email' => 'email@email.com',
            'password' => 'secret',
            'password_confirmation' => 'secret',
        ];

        // if registration is not allowed from config redirect to login
        config()->set('thunderbolt.auth.allow_registration', false);
        $this->post('/register', $payload)
            ->assertRedirect(route('login'))
            ->assertSessionHas('status', 'Registration is not allowed. Ask admin to create an account for you.');

        // if registration allowed
        config()->set('thunderbolt.auth.allow_registration', true);
        $this->post('/register', $payload)
            ->assertStatus(302)
            ->assertRedirect(\config('thunderbolt.auth.redirect_to'));

        unset($payload['password'], $payload['password_confirmation']);
        $this->assertDatabaseHas('users', $payload);
        $this->assertAuthenticated();
        $this->assertAuthenticatedAs(User::where('email', $payload['email'])->first());
    }

    /**
    * if need role permission registered user will get default roles upon registration
    *
    * @test
    */
    public function if_need_role_permission_registered_user_will_get_default_roles_upon_registration()
    {
        $payload = [
            'name' => 'John Doe',
            'email' => 'email@email.com',
            'password' => 'secret',
            'password_confirmation' => 'secret',
        ];

        config()->set('thunderbolt.auth.enable_role_permission', true);
        config()->set('thunderbolt.auth.roles', ['Admin', 'User']);
        config()->set('thunderbolt.auth.default_role', ['User']);

        // add roles
        $this->syncRolesFromConfig();

        $this->post('/register', $payload)
            ->assertStatus(302)
            ->assertRedirect(\config('thunderbolt.auth.redirect_to'));

        $this->assertEquals('email@email.com', auth()->user()->email);
        $this->assertTrue(auth()->user()->hasAllRoles(config('thunderbolt.auth.default_role', [])));

    }

    /**
    * it send confirmation email if config is set to email confirmation
    *
    * @test
    */
    public function it_send_confirmation_email_if_config_is_set_to_email_confirmation()
    {

        $payload = [
            'name' => 'John Doe',
            'email' => 'email@email.com',
            'password' => 'secret',
            'password_confirmation' => 'secret',
        ];

        // set email confirmation setting
        config()->set('thunderbolt.auth.email_confirmation', true);
        $this->assertEquals(true, config('thunderbolt.auth.email_confirmation'));

        Mail::fake();

        $this->post('/register', $payload)
            ->assertStatus(302)
            ->assertRedirect(\config('thunderbolt.auth.redirect_to'));

        // Assert a mailable was not sent...
        Mail::assertQueued(ConfirmationEmail::class);
    }

    /**
    * user can confirm email by clicking on activate email link
    *
    * @test
    */
    public function user_can_confirm_email_by_clicking_on_activate_email_link()
    {
        $user = $this->createUser(['email' => 'test@email.com']);
        $verification = $user->verification()->create(['token' => str_random(32)]);

        $confirmationUrl = route('activate_account', $verification->token);

        $this->get($confirmationUrl)
            ->assertRedirect(route('login'))
            ->assertSessionHas('status', $user->name.' your account has been activated, go ahead an login.');

        $this->assertDatabaseMissing('verifications', $verification->toArray());
    }

    /**
    * user can login if they are active by config
    *
    * @test
    */
    public function user_can_login_if_they_are_active_by_config()
    {
        $user = $this->createUser();

        $user->verification()->create([
            'token' => str_random(32)
        ]);

        $this->post(route('loginPost'), ['email' => 'some@example.com'])
            ->assertSessionHasErrors('password')
            ->assertRedirect();

        $this->assertFalse($this->isAuthenticated());

        // if confirmed email only login
        config()->set('thunderbolt.auth.allow_login_for_confirmed_email', true);

        $payload = ['email' => 'email@example.com', 'password' => 'secret'];

        $this->post(route('loginPost'), $payload)
            ->assertRedirect()
            ->assertSessionHasErrors('email');

        $this->assertFalse($this->isAuthenticated());

        config()->set('thunderbolt.auth.allow_login_for_confirmed_email', false);

        $this->post(route('loginPost'), $payload)
            ->assertRedirect(\config('thunderbolt.auth.redirect_to'));

        $this->assertAuthenticatedAs($user);
    }

    /**
     * User can visit login page
     * @test
     **/
    public function can_visit_login_page()
    {
        // guest can visit login page
        $this->get(\route('login'))
            ->assertStatus(200)
            ->assertSee('Login');
    }

    /**
    * it can log out authenticated user
    *
    * @test
    */
    public function it_can_log_out_authenticated_user()
    {
        $this->loginUser();
        $this->assertTrue($this->isAuthenticated());

        $this->get(route('logout'))
            ->assertRedirect('/');

        $this->assertFalse($this->isAuthenticated());
    }

    /**
    * user an ask for password reset using there registered email
    *
    * @test
    */
    public function user_an_ask_for_password_reset_using_there_registered_email()
    {
        $user = $this->createUser();

        $this->get(route('password.forgot'))
            ->assertStatus(200)
            ->assertSee('email');

        Mail::fake();
        // send password reset
        $payload = ['email' => $user->email];
        $this->post(route('password.email'), $payload)
            ->assertStatus(302)
            ->assertSessionHas('status');
    }

    /**
    * user can reset password using send link
    *
    * @test
    */
    public function user_can_reset_password_using_send_link()
    {
        $user = $this->createUser();

        Mail::fake();
        // send password reset
        $payload = ['email' => $user->email];
        $this->post(route('password.email'), $payload)
            ->assertStatus(302)
            ->assertSessionHas('status');

        $reset  = \DB::table('password_resets')->first();
        $restLink = route('password.reset', ['token' => $reset->token] );
        $this->assertDatabaseHas('password_resets', ['email' => $user->email ]);


        $this->get($restLink)
            ->assertStatus(200)
            ->assertSee('Confirm Password');
    }
}
