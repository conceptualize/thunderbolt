# Zeus Thunderbolt ⚡
This package adds **Roles** and **Permissions** on top of Laravel Auth system with option to customize it using configuration file.

## Getting Started
TBD

## Configuration options
Here are all the configuration options available

```php
'auth' => [
    /**
     * Where should we redirect upon login
     */
    'redirect_to' => '/home',

    /**
     * Whether to confirm registered user email by sending token
     * to confirm. you must configure email sender in config/mail.php
     */
    'email_confirmation' => false,

    /**
     * Subject for welcome message it will contain the email confirmation
     * if above email_confirmation is set to true
     */
    'welcome_email_subject' => 'Welcome to ' . env('APP_NAME', 'Thunderbolt'),

    /**
     * Account link expires in minutes
     */
    'activation_link_expires' => 60*7,

    /**
     * If this is true then user wont be able to login unless user 
     * verifies email account.
     */
    'allow_login_for_confirmed_email' => false,

    /**
     * If you need role and permission you can enable it here
     */
    'enable_role_permission' => false,

    /**
     * List down available roles
     */
    'roles' => [
        'Admin',
        'User'
    ],

    /**
     * Default role which user will get upon registration
     */
    'default_role' => [
        'User'
    ]
]
```

## Licence 
MIT