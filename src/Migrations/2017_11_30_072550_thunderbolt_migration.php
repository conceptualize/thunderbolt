<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ThunderboltMigration extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        // Add is_active flag
//        Schema::table('users', function (Blueprint $table) {
//            $table->boolean('is_active')->after('password')->default(
//                config('thunderbolt.auth.email_confirmation', false)
//            );
//        });

        // Activation table
        Schema::create('verifications', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('user_id');
            $table->string('token');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
//        Schema::table('users', function (Blueprint $table) {
//            $table->dropColumn('is_active');
//        });

        Schema::dropIfExists('verifications');
    }
}
