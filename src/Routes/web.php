<?php

Route::middleware('web')->group( function () {
    // Routes defined here have the web middleware applied
    // like the web.php file in a laravel project
    // They also have an applied controller namespace and a route names.
    Route::get('/login', config('thunderbolt.LoginController').'@showLoginForm')->name('login');
    Route::post('/login', config('thunderbolt.LoginController').'@login')->name('loginPost');
    Route::get('/logout', config('thunderbolt.LoginController').'@logout')->name('logout');

    Route::get('/register', config('thunderbolt.RegisterController').'@showRegistrationForm')->name('register');
    Route::post('/register', config('thunderbolt.RegisterController').'@register');

    Route::get('/activate/{token}', config('thunderbolt.AccountController').'@activate')->name('activate_account');

    Route::get('/password/email', config('thunderbolt.ForgotPasswordController').'@showLinkRequestForm')->name('password.forgot');
    Route::post('/password/email', config('thunderbolt.ForgotPasswordController').'@sendResetLinkEmail')->name('password.email');

    Route::get('/password/reset', config('thunderbolt.ResetPasswordController').'@showResetForm')->name('password.reset');
    Route::post('/password/reset', config('thunderbolt.ResetPasswordController').'@reset')->name('password.resetPost');
});