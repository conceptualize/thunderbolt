<?php

namespace Conceptlz\Zeus\Thunderbolt\Models;

use Illuminate\Database\Eloquent\Model;

class Verification extends Model
{
    protected $guarded = [];

    /**
     * Find a verification by token
     *
     * @param $token
     * @return mixed
     */
    public static function findByToken($token)
    {
        return self::whereToken($token)->first();
    }
    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }
}