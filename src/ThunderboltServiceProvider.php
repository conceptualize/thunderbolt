<?php
namespace Conceptlz\Zeus\Thunderbolt;

use Illuminate\Support\ServiceProvider;
use Conceptlz\Zeus\Thunderbolt\Commands\AddRolesCommand;

class ThunderboltServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        // Publish config
        $this->publishes([
            __DIR__.'/Config/thunderbolt.php' => config_path('thunderbolt.php'),
        ], 'thunderbolt_config');

        // Routes
        $this->loadRoutesFrom(__DIR__ . '/Routes/web.php');

        // Load migration
        $this->loadMigrationsFrom(__DIR__ . '/Migrations');

        // Load translation
        $this->loadTranslationsFrom(__DIR__ . '/Translations', 'thunderbolt');
        $this->publishes([
            __DIR__ . '/Translations' => resource_path('lang/vendor/thunderbolt'),
        ]);

        // Load views
        $this->loadViewsFrom(__DIR__ . '/Views', 'thunderbolt');
        $this->publishes([
            __DIR__ . '/Views' => resource_path('views/vendor/thunderbolt'),
        ]);

        if ($this->app->runningInConsole() && $this->app['config']->get('thunderbolt.auth.enable_role_permission')) {
            $this->commands([
                AddRolesCommand::class
            ]);
        }

        // set auth provider model
        $this->app['config']->set(
            'auth.providers.users.model',
            $this->app['config']->get('thunderbolt.auth_provider_model')
        );
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->mergeConfigFrom(
            __DIR__.'/Config/thunderbolt.php', 'thunderbolt'
        );
    }
}
