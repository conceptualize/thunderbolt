<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta
        name="viewport"
        content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">

    <title>@yield('title', config('app.name'))</title>

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <!-- CSS -->
    @stack('pre_css')
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    @stack('css')
</head>
<body class="@yield('bodyClass')">
@if( $status = session()->get('status') )
    <div class="alert alert-info">
        {{ $status}}
    </div>
@endif

@yield('content')

<!-- JS at bottom -->
@stack('pre_js')
<script src="{{ asset('js/app.js') }}"></script>
@stack('js')
</body>
</html>
