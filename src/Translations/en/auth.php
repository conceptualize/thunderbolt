<?php

return [
    'registratio_not_allowed' => 'Registration is not allowed. Ask admin to create an account for you.',
    'please_confirm_email' => 'Please confirm your email id before login.',
    'email_confirmed' => ':name your account has been activated, go ahead an login.',
    'token_expired' => 'Confirmation link has been expired.',
    'invalid_token' => 'Invalid confirmation link.'
];
