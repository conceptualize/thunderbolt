<?php

return [

    /**
     * Controllers you can use to Extend the auth system, make sure to extend Base controller
     * and make changes there.
     */
    'LoginController' => \Conceptlz\Zeus\Thunderbolt\Controllers\Auth\LoginController::class,
    'AccountController' => \Conceptlz\Zeus\Thunderbolt\Controllers\Auth\AccountController::class,
    'RegisterController' => \Conceptlz\Zeus\Thunderbolt\Controllers\Auth\RegisterController::class,
    'ResetPasswordController' => \Conceptlz\Zeus\Thunderbolt\Controllers\Auth\ResetPasswordController::class,
    'ForgotPasswordController' => \Conceptlz\Zeus\Thunderbolt\Controllers\Auth\ForgotPasswordController::class,

    /**
     * Provider for User model, you should extend the base class for any customization
     */
    'auth_provider_model' => \Conceptlz\Zeus\Thunderbolt\Models\User::class,

    /**
     * Auth related settings
     */
    'auth' => [
        /**
         * Where should we redirect upon login
         */
        'redirect_to' => '/home',

        /**
         * Whether to confirm registered user email by sending token
         * to confirm. you must configure email sender in config/mail.php
         */
        'email_confirmation' => false,

        /**
         * Subject for welcome message it will contain the email confirmation
         * if above email_confirmation is set to true
         */
        'welcome_email_subject' => 'Welcome to ' . env('APP_NAME', 'Thunderbolt'),

        /**
         * Account link expires in minutes
         */
        'activation_link_expires' => 60*7,

        /**
         * If this is true user can visit registration form
         */
        'allow_registration' => true,

        /**
         * If this is true then user wont be able to login unless user
         * verifies email account.
         */
        'allow_login_for_confirmed_email' => false,

        /**
         * If you need role and permission you can enable it here
         */
        'enable_role_permission' => false,

        /**
         * List down available roles and run `php artisan thunderbolt:roles` to seed the roles table
         */
        'roles' => [
            'Admin',
            'User'
        ],

        /**
         * Default role which user will get upon registration
         */
        'default_role' => [
            'User'
        ],

        /**
         * Admin role which will be God role in your system
         */
        'admin_role' => 'Admin',

        /**
         * Available Permissions in this app
         */
        'permissions' => [
            'user' => ['create', 'view', 'update', 'delete'],
            'setting' => ['update']
        ]
    ]
];
