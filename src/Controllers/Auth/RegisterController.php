<?php

namespace Conceptlz\Zeus\Thunderbolt\Controllers\Auth;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Illuminate\Auth\Events\Registered;
use Illuminate\Support\Facades\Validator;
use Conceptlz\Zeus\Thunderbolt\Models\User;
use Illuminate\Foundation\Auth\RegistersUsers;
use Conceptlz\Zeus\Thunderbolt\Mail\ConfirmationEmail;
use Conceptlz\Zeus\Thunderbolt\Controllers\BaseController;

class RegisterController extends BaseController
{
    use RegistersUsers;

    /**
     * Create a new controller instance.
     *
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Show the application registration form.
     *
     * @return \Illuminate\Http\Response
     */
    public function showRegistrationForm()
    {
        if (! config('thunderbolt.auth.allow_registration')) {
            return redirect()->route('login')
                ->with('status', trans('thunderbolt::auth.registratio_not_allowed'));
        }

        return view('thunderbolt::auth.register');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => 'required|max:255',
            'email' => 'required|email|max:255|unique:users',
            'password' => 'required|min:6|confirmed',
        ]);
    }

    /**
     * Handle a registration request for the application.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function register(Request $request)
    {
        if (! config('thunderbolt.auth.allow_registration')) {
            return redirect()->route('login')
                ->with('status', trans('thunderbolt::auth.registratio_not_allowed'));
        }

        $this->validator($request->all())->validate();

        event(new Registered($user = $this->create($request->all())));

        $this->guard()->login($user);

        return $this->registered($request, $user)
                        ?: redirect($this->redirectPath());
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return User
     */
    protected function create(array $data)
    {
        return User::create([
            'name' => $data['name'],
            'email' => $data['email'],
            'password' => bcrypt($data['password']),
        ]);
    }

    /**
     * The user has been registered.
     *
     * @param  Request  $request
     * @param  mixed  $user
     * @return mixed
     */
    protected function registered(Request $request, $user)
    {
        // handle roles if settings asked for it
        if (config('thunderbolt.auth.enable_role_permission')) {
            $default_role = config('thunderbolt.auth.default_role', []);
            $user->assignRole($default_role);
        }

        // send confirmation email
        if (config('thunderbolt.auth.email_confirmation')) {
            $user->verification()->create([
                'token' => str_random(16)
            ]);

            Mail::to($user)->queue(new ConfirmationEmail($user));

            // if only confirmed email is allowed to login then redirect user to login page with status
            if (config('thunderbolt.auth.allow_login_for_confirmed_email')) {
                auth()->logout();

                return redirect()->route('login')
                    ->with('status', trans('thunderbolt::auth.please_confirm_email'));
            }
        }
    }

    /**
     * @return \Illuminate\Config\Repository|mixed
     */
    public function redirectTo()
    {
        return config('thunderbolt.auth.redirect_to', '/home');
    }
}
