<?php

namespace Conceptlz\Zeus\Thunderbolt\Controllers\Auth;

use Conceptlz\Zeus\Thunderbolt\Controllers\BaseController;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;

class LoginController extends BaseController
{
    use AuthenticatesUsers;

    /**
     * Create a new controller instance.
     *
     */
    public function __construct()
    {
        $this->middleware('guest', ['except' => 'logout']);
    }

    public function showLoginForm()
    {
        return view('thunderbolt::auth.login');
    }

    /**
     * The user has been authenticated.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  mixed  $user
     * @return mixed
     */
    protected function authenticated(Request $request, $user)
    {
        if( config('thunderbolt.auth.allow_login_for_confirmed_email') ) {
            if( $user && $user->verification()->count() ) {

                // logout and redirect back
                $this->logout($request);

                return redirect()->back()->withErrors([
                    'email' => trans('thunderbolt::auth.please_confirm_email')
                ])->withInput();
            }
        }
    }

    /**
     * @return \Illuminate\Config\Repository|mixed
     */
    public function redirectTo()
    {
        return config('thunderbolt.auth.redirect_to', '/home');
    }
}
