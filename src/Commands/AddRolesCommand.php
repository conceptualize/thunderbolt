<?php

namespace Conceptlz\Zeus\Thunderbolt\Commands;

use Illuminate\Console\Command;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;

class AddRolesCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'thunderbolt:roles {--p|permissions=true}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Add Thunderbolt roles defined in packages config file';

    /**
     * Create a new command instance.
     *
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        if (config('thunderbolt.auth.enable_role_permission')) {
            $roles = config('thunderbolt.auth.roles', []);

            foreach ($roles as $role) {
                Role::firstOrCreate(['name' => $role]);
            }
        }

        if ($this->option('permissions')) {
            // Add defined permissions also
            $permissions = config('thunderbolt.auth.permissions', []);
            foreach ($permissions as $model => $abilities) {
                foreach ($abilities as $ability) {
                    Permission::firstOrCreate([
                        'name' => strtolower(trim($ability)).'_'.strtolower(trim($model))
                    ]);
                }
            }

            // give all the permissions to admin role
            if ($adminRole = Role::findByName(config('thunderbolt.auth.admin_role'))) {
                $adminRole->syncPermissions(Permission::all());
                $this->info('Full Permissions given to ['.config('thunderbolt.auth.admin_role').'] role');
            }
        }

        $this->warn('Roels added ['.implode(", ", $roles).']');
    }
}
